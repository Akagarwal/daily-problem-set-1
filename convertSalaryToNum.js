const data = require('./data')

const salaryToNum = data.map((eachEmployee)=>{
    
    let salary = eachEmployee['salary'].replace('$', '')
    eachEmployee['salary'] = Number(salary)
    return eachEmployee
})

console.log(salaryToNum);
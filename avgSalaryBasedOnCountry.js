const data = require('./data')
const correctedSalary = require('./correctedSalary')

const updatedData = correctedSalary(data);

const countrySalary = updatedData.reduce((countrySalary, currentValue) => {

    if (countrySalary.hasOwnProperty(currentValue.location)) {
        countrySalary[currentValue.location]['salary'] += Number(currentValue['correctSalary'])
        countrySalary[currentValue.location]['employeeCount'] += 1
        let avg = Number(countrySalary[currentValue.location]['salary'] / countrySalary[currentValue.location]['employeeCount'])
        countrySalary[currentValue.location]['avg'] = avg

    } else {
        let salary = Number(currentValue['correctSalary'])
        countrySalary[currentValue.location] = {}
        countrySalary[currentValue.location]['salary'] = salary
        countrySalary[currentValue.location]['employeeCount'] = 1
        let avg = Number(countrySalary[currentValue.location]['salary'] / countrySalary[currentValue.location]['employeeCount'])
        countrySalary[currentValue.location]['avg'] = avg
    }
    return countrySalary;
}, {})

console.log(updatedData)



console.log(countrySalary)
const data = require('./data')

function correctedSalary(data) {

    const correctSalary = data.map((eachEmployee) => {

        let salary = Number(eachEmployee['salary'].replace('$', ''))
        eachEmployee['correctSalary'] = Number(salary * 10000)
        return eachEmployee
    })

    return correctSalary;
}
// console.log(correctedSalary(data));
module.exports = correctedSalary;
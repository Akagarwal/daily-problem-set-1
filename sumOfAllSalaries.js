const data = require('./data')

let sum = data.reduce((accumulator, currentValue) => {

    const salary = currentValue.salary.replace("$", "")

    return accumulator + Number(salary)

}, 0)

console.log(sum);
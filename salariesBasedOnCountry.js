const data = require('./data')
const correctedSalary = require('./correctedSalary')


const updatedData = correctedSalary(data);

const salaryBasedOnCountry = updatedData.reduce((salaryBasedOnCountry, currentValue) => {

    if (salaryBasedOnCountry.hasOwnProperty(currentValue.location)) {
        let salary = currentValue['correctSalary']
        salaryBasedOnCountry[currentValue.location] += Number(salary);
    } else {
        let salary = currentValue['correctSalary']
        salaryBasedOnCountry[currentValue.location] = Number(salary);
    }
    return salaryBasedOnCountry;
}, {});
console.log(salaryBasedOnCountry)
